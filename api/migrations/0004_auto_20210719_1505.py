# Generated by Django 3.2.4 on 2021-07-19 15:05

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0003_auto_20210719_1404'),
    ]

    operations = [
        migrations.AlterField(
            model_name='extrainfo',
            name='rodzaj',
            field=models.IntegerField(choices=[(0, 'Nieznany'), (3, 'Mistic'), (1, 'Drama'), (4, 'Psychologiczne'), (5, 'Komedia'), (2, 'Sci-FI')], default=0),
        ),
        migrations.CreateModel(
            name='Recenzja',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('opis', models.TextField(default='')),
                ('gwiazdki', models.IntegerField(default=5)),
                ('film', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='api.film')),
            ],
        ),
    ]
