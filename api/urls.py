from django.urls import include, path
from rest_framework import routers
from api import views

router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'filmy', views.FilmViewSet, basename="film")
router.register(r'recenzje', views.RecenzjaViewSet, basename="recenzje")
router.register(r'aktorzy', views.AktorzyViewSet, basename="aktorzy")


urlpatterns = [
    path('', include(router.urls)),
]
